const Score = require('./middlewares/score');

module.exports = app => {
  app.post('/api/score', Score.setScore);
  app.get('/api/score', Score.getScore);
};
