class Score {
  static getScore(req, res) {
    res.json(req.store.getStore());
  }

  static setScore(req, res) {
    const score = {
      name: req.body.name,
      terns: req.body.terns
    };
    if (!score) {
      res.status(400).json({
        reason: 'Score is required.',
      });
    }
    req.store.setStore(score);
    res.json(score);
  }
}

module.exports = Score;
