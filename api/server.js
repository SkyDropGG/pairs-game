const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const store = require('./middlewares/store');

const app = express();
let staticPath = path.join(__dirname, '..', 'build');
app.use(express.static(staticPath));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(store);

require('./routes')(app);

app.use((req, res) => {
  const indexHtml = path.join(staticPath, 'index.html');
  res.sendFile(indexHtml);
});

app.listen(process.env.PORT || 8080);
