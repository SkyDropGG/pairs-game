const storage = require('./utils/storage');

class Store {
  validateData(data) {
    return {
      terns: data.terns || 0,
      name: data.name || ''
    };
  }

  setStore(value) {
    storage.push(this.validateData(value));
  }

  getStore() {
    return storage;
  }
}

module.exports = (req, res, next) => {
  req.store = new Store();
  return next();
};
